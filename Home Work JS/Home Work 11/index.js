// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)





// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const btnStatus = document.querySelectorAll('.fas')
const form = document.querySelector('form')
const inputs = document.querySelectorAll('form input')
const confirm = document.querySelector('.btn')

console.log(form)
console.log(inputs)
console.log(confirm)


	form.addEventListener('submit', (event) => {
		event.preventDefault()
		const inputs = event.target.querySelectorAll('input')
		console.log(inputs)
	
		const val = {}
		inputs.forEach(input => {
	
			console.log(input)
			val[input.name] = input.value
		})
		console.log(val)
	})




btnStatus.forEach((elem) => {
	elem.addEventListener('click',(event) => {
	const input = elem.previousElementSibling
	
	if (input.type === "text"){
		input.type = "password"
		event.target.classList.replace('fa-eye-slash', 'fa-eye')
	}else{
		input.type = "text"
		event.target.classList.replace('fa-eye', 'fa-eye-slash')
	}
})
})

confirm.addEventListener('click', (event) =>{

	const inputError = document.querySelector('.input-error')
	if (inputError.innerHTML != ""){

		inputError.innerHTML = ""
	}
	

	if(form.elements.password.value === form.elements.repeat_password.value) {
	
		alert("You are welcome")
	}

	else {
		inputError.innerHTML = "Потрібно ввести однакові значення"
	}
	
})
