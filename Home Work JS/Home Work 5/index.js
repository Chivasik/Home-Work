// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.


// const createNewUser = () => {

// 	const firstName = prompt('Enter the first name');
// 	const lastName = prompt('Enter the last name');

// 	 const newUser = {

// 		firstName,
// 		lastName,
// 		getLogin(){

// 			return firstName[0].toLowerCase() + lastName.toLowerCase();
// 		}

// 	 }
// 	 return newUser;
	
// }

// console.log (createNewUser().getLogin())


