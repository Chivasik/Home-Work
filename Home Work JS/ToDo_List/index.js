// const button = document.getElementById('button')
// const listContainer = document.getElementById('todo-list')
// const input = document.getElementById('inputId')

// button.addEventListener('click', ()=>{

// 	 const task = input.value
// 	 console.log(task)
// 	 if (task !== ''){
// 		const li = document.createElement('li')
// 		li.classList.add ('todoListItem')
// 		li.textContent = task
// 		listContainer.appendChild(li)
// 	 }
// })

// listContainer.addEventListener('click', (event) => {
// 	if (event.target.tagName === 'LI') {

// 	  event.target.classList.toggle('completed');
// 	}

// })

function todoApp() {
  // 1. CreateTitle() Создание заголовка
  // 2. Форма CreateForm() Создание формы для ввода задачи
  // 3. CreateList() Создает  список задач
  // 4. CreateListItem создаем элемент списка задач

  const title = createTitle("ToDo list");
  const form = createForm();
  const list = createList();
  const itemsList = JSON.parse(localStorage.getItem('key')) ? JSON.parse(localStorage.getItem('key')) : []
  itemsList.forEach((elem) => {
    const listItem = createListItem(elem.text);
	 if(elem.done) listItem.item.classList.add('done')
    list.append(listItem.item);
  });

  form.form.addEventListener("submit", (event) => {
    event.preventDefault();
    if (form.input.value.trim() !== "") {
      const listItem = createListItem(form.input.value);
      list.append(listItem.item);
      form.input.value = "";
      updateLocalStorage();
    }
  });

  document.body.append(title, form.form, list);
}

function createTitle(name) {
  const title = document.createElement("h1");
  title.innerText = name;
  title.classList.add("page-title");
  return title;
}

function createForm() {
  const form = document.createElement("form");
  const input = document.createElement("input");
  const button = document.createElement("button");
  input.placeholder = "Enter Task";
  button.innerText = "ADD";
  input.classList.add("input");
  button.classList.add("btn");
  form.classList.add("input-wrraper");

  form.append(input, button);

  return { form, input, button };
}

function createList() {
  const list = document.createElement("ul");
  list.classList.add("todo-list");
  return list;
}

function createListItem(text) {
  const item = document.createElement("li");
  const itemText = document.createElement("p");
  const wrapper = document.createElement("div");
  const btnDone = document.createElement("button");
  const btnDel = document.createElement("button");

  item.classList.add("todo-list-item");
  btnDel.classList.add("btn-delete");
  btnDone.classList.add("btn-done");

  btnDel.innerText = "Delete";
  btnDone.innerText = "Done";
  itemText.innerText = text;

  item.append(itemText, wrapper);
  wrapper.append(btnDone, btnDel);

  btnDel.addEventListener("click", () => {
    if (confirm("Delete?")) {
		item.remove();
		updateLocalStorage();
	 } 
	 

  });

  btnDone.addEventListener("click", () => {
    item.classList.toggle("done");
	 updateLocalStorage();

  });

  return { item, itemText, btnDone, btnDel };
}

function updateLocalStorage() {
  const items = document.querySelectorAll("li");
  console.log(items[0].firstChild.innerText);

  const itemsList = [];
  items.forEach((item) => {

	const text = item.firstChild.innerText
	const done = item.classList.contains('done')

	const obj = {text, done}
	itemsList.push(obj)
   
  });
  localStorage.setItem("key", JSON.stringify(itemsList));


  // console.log(itemsList)
  // console.log(JSON.parse(localStorage.getItem('key')))
  // console.log(localStorage.getItem('key'))
}

todoApp();

// document.body.getElementsByTagName('') HTMLCollection
// document.body.querySelectorAll() NODE List
