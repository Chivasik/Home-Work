document.body.addEventListener("keydown", (event) => {
  console.log(event.key);
  const key = event.key.toLowerCase();
  const element = document.getElementById(key);
  console.log(element);

  const buttons = document.querySelectorAll(".btn");
  buttons.forEach((elem) => {
    elem.classList.remove("active");
  });

  if (element) {
    element.classList.add("active");
  }
});
