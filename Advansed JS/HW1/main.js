// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


class Employee {

	constructor(name, age, salary){
		this._name = name
		this._age = age
		this._salary = salary
	}
	get name() {
		return this._name;
	 }
	 get age() {
		return this._age;
	 }
	 get salary() {
		return this._salary;
	 }

	 set name(value) {
		if (value.length < 4) {
		  alert("Имя слишком короткое.");
		  
		}else{
			return this._name;
			
		}
		
	 }

	 set age(value) {
		if (value < 100) {
		  alert("Люди столько не живут");
		  return;
		}
		this._age = value;
	 }
	 set salary(value) {

		this._salary = value;
	 }
}
class Programmer extends Employee {

	constructor (name, age, salary, lang){
		super (name, age, salary)
		this._lang = lang;
		
	}
	get salary() {
		return this._salary * 3;
	 }
	 get lang(){
		return this._lang;
	 }
}
let user = new Programmer("Vasiya", "45", "5000", "C#");
let user2 = new Programmer("Katia", "35", "4000", "JAVA");
let user3 = new Programmer("Ira", "25", "6000", "JS");

console.log(user.name);
console.log(user.age);
console.log(user.salary);
console.log(user.lang);

console.log(user2.name);
console.log(user2.age);
console.log(user2.salary);
console.log(user2.lang);

console.log(user3.name);
console.log(user3.age);
console.log(user3.salary);
console.log(user3.lang);



